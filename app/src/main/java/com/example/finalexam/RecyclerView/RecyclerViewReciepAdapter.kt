package com.example.finalexam.RecyclerView;

import android.view.LayoutInflater
import android.view.View;
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide
import com.example.finalexam.R

class RecyclerViewReciepAdapter(private  val list: List<Reciep>):
    RecyclerView.Adapter<RecyclerViewReciepAdapter.ReciepViewHolder>() {

    class ReciepViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var foodImage: ImageView
        var foodName: TextView
        var foodTitle: TextView

        init {
            foodImage = itemView.findViewById(R.id.foodImage)
            foodName = itemView.findViewById(R.id.foodName)
            foodTitle = itemView.findViewById(R.id.foodTitle)
        }
        fun setData(reciep: Reciep) {

            Glide.with(foodImage)
                .load(reciep.imageUrl)
                .into(foodImage)
            foodName.text = reciep.name
            foodTitle.text = reciep.title


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReciepViewHolder {
        val reciepView=
        LayoutInflater.from(parent.context).inflate(R.layout.reciep_item, parent, false)
        return ReciepViewHolder(reciepView)
    }

    override fun onBindViewHolder(holder: ReciepViewHolder, position: Int) {
        val p = list[position]
        holder.setData(p)
     }

    override fun getItemCount()= list.size

}
