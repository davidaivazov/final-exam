package com.example.finalexam.RecyclerView

data class Reciep(
    var id: Int,
    var imageUrl: String,
    var title: String,
    var name: String
)
