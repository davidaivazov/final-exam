package com.example.finalexam.fragment_menu

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalexam.R
import com.example.finalexam.RecyclerView.Reciep
import com.example.finalexam.RecyclerView.RecyclerViewReciepAdapter
import com.example.finalexam.fragments_profile.MenuFragmentDirections

class SeaFoodFragment: Fragment(R.layout.fragment_seafood) {
    private lateinit var buttonBack: Button
    private lateinit var recyclerViewReciepAdapter: RecyclerViewReciepAdapter
    private lateinit var recyclerView: RecyclerView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val controller = Navigation.findNavController(view)
        buttonBack = view.findViewById(R.id.buttonBack)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerViewReciepAdapter = RecyclerViewReciepAdapter(getData())
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = recyclerViewReciepAdapter

        buttonBack.setOnClickListener() {
            val action =
                SeaFoodFragmentDirections.actionSeaFoodFragmentToMenuFragment()
            controller.navigate(action)
        }

    }

    private fun getData(): List<Reciep> {
        val reciepList = ArrayList<Reciep>()

        reciepList.add(
            Reciep(
                1,
                "https://gemrielia.ge/media/__sized__/images/oraguli-crop-c0-5__0-5-450x301.png",
                "ჩახეხეთ მდოგვში 4 კბილი ნიორი და ლიმონის ცედრა. შემდეგ. ჩაწურეთ ლიმონის წვენი. მოაყარეთ მწიკვი მარილი, წვრილად დაჭრილი კამა, 2 სკ ზეითუნის ზეთი. კარგად მოურიეთ და წაუსვით საცხობ ქაღალდზე მოთავსებულ ორაგულის ნაჭრებს." +
                        " დაუდეთ ირგვლივ 4 ნაჭერი ლიმონი და შედგით ღუმელში. 450 გრადუსზე, 10 წუთით. ",
                 "ორაგულის ფელე",


            )
        )
        reciepList.add(
            Reciep(
                2,
                "https://gemrielia.ge/media/__sized__/images/chaxoxbili_JJkzlYD-crop-c0-5__0-5-450x301-70.jpg",
                "ქარიყლაპია,ლოქო,წვერა,კობრი ან სხვა რომელიმე თევზი გავასუფთავოთ, მოვაყაროთ მარილი და დავაწყოთ ღრმა ტაფაზე. მოვაყაროთ ალყა-ალყა დაჭრილი ხახვი, წვრილად დაჭრილი ქინძი და ოხრახუში, დავასხათ ზეთი და 2 ჩაის ჭიქა პომიდვრის წვენი;  " +
                        "ჩავაყოლოთ სტაფილო,დაფნის ფოთოლი, 20-25 მარცვალი შავი პილპილი და ვხარშოთ ნელ ცეცხლზე .მოხარშული თევზის ნაჭრები დავალაგოთ ღრმა ლანგარზე და ზედ მოვასხათ თავისივე წვენი.",
                "თევზის ჩახოხბილი",


                )
        )
        reciepList.add(
            Reciep(
                3,
                "https://gemrielia.ge/media/__sized__/images/kotletebi-crop-c0-5__0-5-450x301-70.jpeg",
                "დარბილებულ კარაქს შეურიეთ დაჭრილი მწვანილი, დაყავით პატარა ნაწილებად, გააკეთეთ ბურთულები და გააცივეთ. სკუმბრიას ფილე გაატარეთ ხორცსაკეპ მანქანაში, მოაყარეთ პილპილი, მარილი და კარგად აურიეთ. თევზიანი მასა გაყავით პატარ-პატარა ნაწილებად და გააბრტყელეთ მრგვლად." +
                        " მასის შუაში მოათავსეთ კარაქის ბურთულები, შეაერთეთ ბოლოები, ამოავლეთ დაფხვნილ ორცხობილაში და მიეცით კატლეტის ფორმა. დაბრაწეთ ზეთში, შემდეგ შედგით ღუმელში 5-7 წთ. მიირთვით მოხარშულ კარტოფილთან და ტომატთან.",
                "თევზის კატლეტი",


                )
        )
        reciepList.add(
            Reciep(
                4,
                "https://gemrielia.ge/media/__sized__/images/img15346.768x512-crop-c0-5__0-5-450x301-70.jpg",
                "1 კგ გასუფთავებული და გარეცხილი თევზი დაჭერით. მოაყარეთ მარილი, გააჩერეთ 1 სთ.\n" +
                        "თევზი ჩადეთ ალუმინის ქვბაში, ზედ დააწყვეთ შავი პილპილის მარცვლები, 4 ც დაფნის ფოთოლი,\n" +
                        "დაასხით 100 გრ ზეთი, 500 გრ პომიდვრის წვენი. დაუმატეთ წიწაკა, მარილი გემოვნებით. შედეთ\n" +
                        "ღუმელში 1 სთ-ით.",
                "თევზი პომიდორში",


                )
        )
        reciepList.add(
            Reciep(
                5,
                "https://gemrielia.ge/media/__sized__/images/tevzi_bajeshi1-crop-c0-5__0-5-450x301-70.jpg",
                "თევზს მოვაყაროთ მარილი და ზეთში შევწვათ. შეგვიძლია წინასწარ ფქვილში ამოვავლოთ და ისე შევწვათ. ნიგოზი, ნიორი, ქინძი, ხახვი და სანელებლები ავურიოთ და ანადუღარი, ცივი წყლის დამატებით ბლენდერით ავთქვიფოთ. წყალი თანდათან დავუმატოთ სასურველი კონსისტენციის მიღებამდე. შემწვარ თევზს დავასხათ სოუსი, გავაჩეროთ 30 წუთის განმავლობაში და ისე მივიტანოთ\n" +
                        "სუფრაზე.",
                "თევზი ნიგვზით",


                )
        )
        reciepList.add(
            Reciep(
                6,
                "https://gemrielia.ge/media/__sized__/images/tevziludiskliarshi-crop-c0-5__0-5-450x301-70.jpg",
                "კვერცხი, პილპილი და მარილი გავთქვიფოთ, დავუმატოთ ნელ-ნელა ფქვილი. როცა მასა ერთგვაროვანი გახდება, დავასხათ 1 ს.კ. ზეთი და ლუდი. უნდა მივიღოთ თხელი ცომის მასა.\n" +
                        "ცომში ამოვავლოთ თევზის ნაჭრები და კარგად გაცხელებულ ზეთში ორივე მხრიდან შევწვათ.\n" +
                        "თევზი სუფრაზე სალათის ფოთლების, ბროწეულის მარცვლებისა და ნარშარაბის თანხლებით მივიტანოთ.",
                "თევზი ლუდის კლიარში",


                )
        )
        reciepList.add(
            Reciep(
                7,
                "https://gemrielia.ge/media/__sized__/images/oragulispashteti4-crop-c0-5__0-5-450x301-70.jpg",
                "ორივენაირი ორაგული, არაჟანი, ხახვი, მარილი, პილპილი და ლიმონის წვენი ჩოპერში დავაქუცმაცოთ. შეგვიძლია თევზი და ხახვი გავატაროთ ხორცსაკეპ მანქანაში და შემდეგ დავუმატოთ დანარჩენი ინგრედიენტები.\n" +
                        "პაშტეტს მოვაყაროთ მწვანე ხახვი და მივირთვათ გახუხულ პურთან ერთად.",
                "ორაგულის პაშტეტი",


                )
        )
        reciepList.add(
            Reciep(
                8,
                "https://gemrielia.ge/media/__sized__/images/oraguli_7cX5hu2-crop-c0-5__0-5-450x301-70.jpg",
                "გსოიოს სოუსში აურიეთ თაფლი და მდოგვი.\n" +
                        "მოათავსეთ ორაგულის ფილე პერგამენტდაფენილ გამოსაცხობ ფირფიტაზე (თუ კანიანი ფილეა, დადეთ კანით დაბლა), მოასხით სოიოს სოუსი და მოათვსეთ 180-190 გრადუსზე გახურებულ ღუმელში 10-12 წუთით.\n" +
                        "სუფრასთან მიიტანეთ რუკოლას სალათის ფოთლებთან ერთად, მოასხით გამოცხობის შემდეგ დარჩენილი წვენი პირდაპირ გამოსაცხობი ფირფიტიდან.\n" +
                        "გემრიელად მიირთვით!",
                "ორაგული აზიურად",


                )
        )
        reciepList.add(
            Reciep(
                9,
                "https://gemrielia.ge/media/__sized__/images/spagetimidiebit5-crop-c0-5__0-5-450x301-70.jpg",
                "სპაგეტი მოვხარშოთ მდუღარე მარილიან წყალში შეფუთვაზე მითითებული წესის მიხედვით და გადავწუროთ. ხახვი წვრილად დავჭრათ და ზეთში მოვშუშოთ, დავუმატოთ ნაღები, მარილი, პილპილი და მიდიები. ვშუშოთ 1-2 " +
                        "წუთი და შევურიოთ სპაგეტი. ბოლოს მოვაწუროთ ლიმონი და მოვახეხოთ ცედრა.",
                "სპაგეტი მიდიებით",


                )
        )
        reciepList.add(
            Reciep(
                10,
                "https://gemrielia.ge/media/__sized__/images/skumbria-crop-c0-5__0-5-450x301-70.jpg",
                "დანაყეთ ან წვრილად დაჭერით ნიორი და შეურიეთ ნაღებსა და კეფირს. წვრილ სახეხზე გახეხეთ ყველი და შეურიეთ საფანელი." +
                        "კარგად გარეცხეთ და გაასუფთავეთ თევზი და დაჭერით თანაბარ ნაჭრებად." +
                        "მოათავსეთ თევზი გამოსაცხობ ფორმაზე, მოაყარეთ მარილი და პილპილი." +
                        "მოასხით  ნაღებისა და კეფირის სოუსი და მოაყარეთ ყველისა და საფანელის ნარევი." +
                        "გაახურეთ ღუმელი 220 გრადუსზე და მოათავსეთ თევზი გამოსაცხობად 25-30 წუთი." +
                        "მიიტანეთ კარტოფილთან და ბოსტნეულის სალათთან ერთად.\n" +
                        "გემრიელად მიირთვით!",
                "სკუმბრია ყველში",


                )
        )
        reciepList.add(
            Reciep(
                11,
                "https://gemrielia.ge/media/__sized__/images/shamaia-crop-c0-5__0-5-450x301-70.jpg",
                "ფქვილში ავურიოთ მარილი, ამოვავლოთ თევზები და გაცხელებულ ზეთში შევწვათ. ქინძი, ძმარი, მარილი, წიწაკა და ნიორი ბლენდერით მსუბუქად ავთქვიფოთ, გავაზავოთ წყლით. თევზი სუფრაზე ქინძმართან ერთად მივიტანოთ.",
                "შამაია ქინძმარში",


                )
        )
        reciepList.add(
            Reciep(
                12,
                "https://gemrielia.ge/media/__sized__/images/1_S4SobYY-crop-c0-5__0-5-450x301-70.jpg",
                "გარეცხეთ კარგად თევზი, გაასუფთავეთ, მოაჭერით თავი და კუდი. შეამშრალეთ ქაღალდის ხელსახოცით." +
                        "მოაყარეთ მარილი და წაუსვით სოუსი." +
                        "სოუსითვის უბრალოდ შეურიეთ ერთმანეთში მდოგვი და მაიონეზი." +
                        "მოათავსეთ თევზის ნაჭრები ფოლგადაფენილ ლითნის ფირფიტაზე და შედგით ღუმელში ან აეროგრილში." +
                        "შეწვით 180 გრადუსზე დაახლოებით 30 წუთის განმავლობაში.",
                "სკუმბრია სოუსში",


                )
        )
        reciepList.add(
            Reciep(
                13,
                "https://gemrielia.ge/media/__sized__/images/stories/lela_modeb/moiva-crop-c0-5__0-5-450x301-70.jpg",
                "გარეცხეთ თევზი და დაწურეთ. შეამშრალეთ." +
                        "ცალკე ჯამში ერთმანეთში აურიეთ ფქვილი, დაქფული პილპილი და მარილი." +
                        "ღუმელის ფირფიტაზე დააფინეთ ფოლგა ან პერგამენტი. თითოეული თევზი ამოავლეთ ფქვილის მასაში და დაალაგეთ თევზი ფირფიტაზე მჭიდროდ." +
                        "ზეთი არ მოასხათ!" +
                        "შედგით ფირფიტა ღუმელში და აცხვეთ 15 წუთი 200 გრადუსზე.\n" +
                        "გემრიელად მიირთვით!",
                "მოივა ღუმელში",


                )
        )
        reciepList.add(
            Reciep(
                14,
                "https://gemrielia.ge/media/__sized__/images/stories/lela_modeb/kalmaxi-crop-c0-5__0-5-450x301-70.jpg",
                "გასუფთავებულ თევზს მოვაყაროთ მარილი და პილპილი და შევწვათ ორივე მხრიდან კარაქში. დარჩენილ ცხიმს დავუმატოთ ცოტა კარაქი და თხლად დაჭრილი სტაფილო, ბულგარული წიწაკა, ხახვი და ლიმონი მოვშუშოთ.\n" +
                        "შეგრილებულ თევზს მოვაყაროთ მოშუშული ბოსტნეული და ბლომად ბროწეული. დავაჭრათ კამა.",
                "კალმახი ბროწეულით",


                )
        )
        reciepList.add(
            Reciep(
                15,
                "https://gemrielia.ge/media/__sized__/images/trout-with-herb-butter-recipe-9-547x365_3fe25-crop-c0-5__0-5-450x301-70.jpg",
                "კალმახის ფილე კარგად ამოვსვაროთ  მარილში და შავ წიწაკაში. ტაფა გავაცხელოთ, დავასხათ ცოტა ზეთი და ფილე შევწვათ ორივე მხრიდან ისე რომ კარგად შეიბრაწოს.\n" +
                        "სანამ თევზს ვწვავთ, 4ს/კ კარაქი, 2ს/კ ლიმონის წვენი და 3ს/კ დაჭრილი ოხრახუში კარგად შევურიოთ ერთმანეთს. მიღებული მასა წავუსვათ შემწვარ თევზის ფილეს სანამ ცხელია. გემრიელად მიირთვით ",
                "კალმახის ფილე",


                )
        )


        return reciepList
    }

}