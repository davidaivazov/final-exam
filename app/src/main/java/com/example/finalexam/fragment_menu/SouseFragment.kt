package com.example.finalexam.fragment_menu

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalexam.R
import com.example.finalexam.RecyclerView.Reciep
import com.example.finalexam.RecyclerView.RecyclerViewReciepAdapter
import com.example.finalexam.fragments_profile.MenuFragmentDirections

class SouseFragment: Fragment(R.layout.fragment_souse) {
    private lateinit var buttonBack: Button
    private lateinit var recyclerViewReciepAdapter: RecyclerViewReciepAdapter
    private lateinit var recyclerView: RecyclerView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val controller = Navigation.findNavController(view)
        buttonBack = view.findViewById(R.id.buttonBack)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerViewReciepAdapter = RecyclerViewReciepAdapter(getData())
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = recyclerViewReciepAdapter

        buttonBack.setOnClickListener() {
            val action =
                SouseFragmentDirections.actionSouseFragmentToMenuFragment()
            controller.navigate(action)
        }

    }

    private fun getData(): List<Reciep> {
        val reciepList = ArrayList<Reciep>()

        reciepList.add(
            Reciep(
                1,
                "https://gemrielia.ge/media/__sized__/images/maionezi_1ox5FSC-crop-c0-5__0-5-450x301-70.jpg",
                "ჩაასხით ქილაში ოთახის ტემპერატურის რძე, დაუმატეთ ზეთი და დააბლენდერეთ. \n" +
                        "დაუმატეთ მდოგვი, ლიმონის წვენი და ნივრის ფხვნილი.\n" +
                        "ათქვიფეთ ყველა ინგრედიენტი იქამდე, სანამ მასა არ გასქელდება.\n" +
                        "სულ ეს არის! გემრიელი შინაური მაიონეზი მზად არის.. გემრიელად მიირთვით!",
                "მსუბუქი მაიონეზი",


                )
        )
        reciepList.add(
            Reciep(
                2,
                "https://gemrielia.ge/media/__sized__/images/avokadosmaionezi1-crop-c0-5__0-5-450x301-70.jpg",
                "ავოკადოს რბილობი მოათავსეთ ბლენდერში.\n" +
                        "დაუმატეთ ყველა დანარჩენი ინგრედიენტი და ათქვიფეთ ერთგვაროვანი მასის მიღებამდე.\n" +
                        "მზა მაიონეზი შეინახეთ მაცივარში, ჰერმეტულად დახურულ მინის ჭურჭელში არა უმეტეს 3 დღისა.",
                "ავოკადოს მაიონეზი",


                )
        )
        reciepList.add(
            Reciep(
                3,
                "https://gemrielia.ge/media/__sized__/images/shindissacebeli5-crop-c0-5__0-5-450x301-70.jpg",
                "შინდს დავასხათ წყალი და მოვხარშოთ, შემდეგ გავაგრილოთ და გადავწუროთ.\n" +
                        "ხილი მოვათავსოთ თუშფალანგზე და გავხეხოთ. ხეხვისას თან ვასხათ თავისი ნახარში, კარგად რომ გაიხეხოს.\n" +
                        "ქინძი და ნიორი დავკეპოთ და მარილთან ერთად შევურიოთ გახეხილი შინდის მასას.\n" +
                        "თუ საწებელი სქელი იქნება, თავისი წვენით გავათხელოთ.",
                "შინდის საწებელი",


                )
        )
        reciepList.add(
            Reciep(
                4,
                "https://gemrielia.ge/media/__sized__/images/tomatissousi4-crop-c0-5__0-5-450x301-70.jpg",
                "ზეთში მოვშუშოთ ნიორი, დავუმატოთ დაქუცმაცებული პამიდორი, ხმელი ბაზილიკი და ვადუღოთ, სანამ ზედმეტი წვენი დაშრება, ბოლოს დავუმატოთ მარილი და წიწაკა. გამოვიყენოთ გაგრილებული.",
                "იტალიური პიცის სოუსი",


                )
        )
        reciepList.add(
            Reciep(
                5,
                "https://gemrielia.ge/media/__sized__/images/tomatissousi-crop-c0-5__0-5-450x301-70.jpg",
                "გარჩეული ნიორი, წიწაკა და პამიდორი გავატაროთ ხორცსაკეპ მანქანაში, წვრილ ბადეში.\n" +
                        "დავუმატოთ დაფქული წიწაკა, მარილი, გადავიტანოთ მინის ან მომინანქრებულ ჭურჭელში და დავდგათ დასადუღებლად 24\n" +
                        "საათი. თან ხშირად ვურიოთ. გადავიტანოთ მშრალ, სტერილურ ქილაში და შევინახოთ მაცივარში.",
                "ცხარე საწიბელი",


                )
        )
        reciepList.add(
            Reciep(
                6,
                "https://gemrielia.ge/media/__sized__/images/1343378943_sous-karbonara-4-crop-c0-5__0-5-450x301-70.jpg",
                "გაახურეთ ზეთი, მოხრაკეთ ნიორი და წვრილად დაჭრილი ლორი\n" +
                        "\n" +
                        "ათქვიფეთ ნაღები კვერცხის გულთან ერთად და დაუმატეთ მასას.\n" +
                        "\n" +
                        "თუშეთ დაბალ ცეცხლზე და თან გამუდმებით ურიეთ. შემდეგ დაუმატეთ ყველი და მარილი გემოვნებით.",
                "კარბონარას სოუსი",


                )
        )
        reciepList.add(
            Reciep(
                7,
                "https://gemrielia.ge/media/__sized__/images/ajika5-crop-c0-5__0-5-450x301-70.jpg",
                "ყველა ინგრედიენტი(ნიგოზი - 1 ჭიქა, ქინძი - 1 კონა,წიწაკა ჩილი - 2 ცალი, ნიორი - 10 კბილი,შაქარი - 1 ს/კ,მარილი - 1 ჩ/კ) მოათავსეთ ბლენდერის ჭიქაში და ათქვიფეთ.\n" +
                        "გადაიტანეთ ჰერმეტულ ჭურჭელში, დააფარეთ მჭიდროდ თავსახური და შეინახეთ მაცივარში.",
                "ცხარე აჯიკა ნიგვზით\n",


                )
        )
        reciepList.add(
            Reciep(
                8,
                "https://gemrielia.ge/media/__sized__/images/arajnissousi-crop-c0-5__0-5-450x301-70.jpg",
                "აიღეთ ახალი არაჟანი, კამა გარეცხეთ და გააშრეთ. ნიორი გაარჩიეთ.\n" +
                        "ნიორი გაატარეთ პრესში, კამა დაჭერით წმინდად.\n" +
                        "დაუმატეთ არაჟანს ნიორი და კამა, მარილი გემოვნებით და აურიეთ.\n" +
                        "სუფრასთან მიიტანეთ ნებისემირ ხორცეულთან და თევზთან ერთად.\n" +
                        "\n" +
                        "გემრიელად მიირთვით!",
                "არაჟნის სოუსი",


                )
        )
        reciepList.add(
            Reciep(
                9,
                "https://gemrielia.ge/media/__sized__/images/kliavis_sousi-crop-c0-5__0-5-450x301-70.jpg",
                "ქლიავი გარეცხეთ, ამოაცალეთ კურკები. \n" +
                        "გაფცქვენით 2-3 თავი ნიორი. წიწაკას მოაჭერით ყუნწი და გაასუფთავეთ თესლისგან.\n" +
                        "გაატარეთ ქლიავი, ნიორი და წიწაკა ხორცსაკეპში, დაუმატეთ მარილი, შაქარი და სხვა სანელებლები.\n" +
                        "დადგით მიღებული მასა ცეცხლზე, აადუღეთ და ხარშეთ 30 წუთის განმავლობაში.\n" +
                        "გაანაწილეთ სტერილურ ქილებში და მოხუფეთ. შეინახეთ გრილ ადგილას.",
                "ქლიავის ტყემალი",


                )
        )
        reciepList.add(
            Reciep(
                10,
                "https://gemrielia.ge/media/__sized__/images/sou-crop-c0-5__0-5-450x301.png",
                "წიწაკა დაჭერით კუბიკებად, პომიდორს გააცალეთ კანი და ნიორთან ერთად დაჭერით წვრილად. ყველი დაჭერით თხლად 2-3 მმ სიგანეზე. ქვაბში მოათავსეთ  ნიორი და მწარე წიწაკა, შეწვით 2-3 წუთით.\n" +
                        "\n" +
                        "შემდეგ დაუმატეთ პომიდორი და ასევე გააჩერეთ 2-3 წუთით, დაამატეთ 100 მლ რძე და წამოადუღეთ. ნელ-ნელა დაამატეთ ყველი. დარჩენილი რძე ჩაასხით სოუსში.",
                "ცხარე ყველის სოუსი",


                )
        )
        reciepList.add(
            Reciep(
                11,
                "https://gemrielia.ge/media/__sized__/images/pesto-crop-c0-5__0-5-450x301-70.jpg",
                "ყველა ინგრედიენტი(კამა - 60 გ,ზეთი - 80 გ,გოგრის თესლი - 50 გ (ან ნიგოზი),ნიორი - 2 კბილი,1/2 ლიმონის ცედრა,1/2 ლიმონის წვენი,მარილი - 1/3 ჩკ,წიწაკა ჩილი - 1/2,გახეხილი ყველი, მაგარი სახეობის - 15 გ," +
                        ") მოათავსეთ ჩოპერში და კარგად დააქუცმაცეთ. ერთგვაროვან მასას რომ მიიღებთ, მზად არის.\n" +
                        "სოუსი გადაიტანეთ მინის ქილაში და შეინახეთ მაცივარში.\n" +
                        "სოუსი იდეალურია თევზთან და სალათებში.",
                "პესტოს სოუსი კამით",


                )
        )
        reciepList.add(
            Reciep(
                12,
                "https://gemrielia.ge/media/__sized__/images/sawebeli_SMFfPLC-crop-c0-5__0-5-450x301-70.jpg",
                "კამა და ნიორი დაჭერით, დაუმატეთ 1 ლიმონის ცედრა და წვენი, მარილი და ზეითუნის ზეთი.\n" +
                        "ეს ყველაფერი ხელის ბლენდერით დააბლენდერეთ და სულ ეს არის.\n" +
                        "გადაიტანეთ საწებელი მინის ქილაში და შეინახეთ მაცივარში 2-3 თვემდე.",
                "მწვანე საწებელი",


                )
        )
        reciepList.add(
            Reciep(
                13,
                "https://gemrielia.ge/media/__sized__/images/guruli_suneli-crop-c0-5__0-5-450x301-70.jpg",
                "გემოვნების მიხედვით შეარჩიეთ ინგრედიენტების(ქინძი,ნიორი,მარილი,წიწაკა დაფქული,თხილი ან ნიგოზი,ზეითუნის ზეთი - სურვილისამებრ) დოზები. ყველა მათგანი ერთად კარგად დანაყეთ. სურვილისამებრ" +
                        " შეურიეთ ზეითუნის ზეთი (ზოგჯერ ძმარსაც უმატებენ).\n" +
                        "\n" +
                        "სულ ეს არის. გააგემრიელეთ თქვენი სალათები ამ სუნელით.",
                "გურული სუნელი",


                )
        )
        reciepList.add(
            Reciep(
                14,
                "https://gemrielia.ge/media/__sized__/images/aluCa-crop-c0-5__0-5-450x301-70.jpg",
                "წამოვადუღოთ ალუჩა, გადავწუროთ. გავხეხოთ სახეხზე და დავუმატოთ ხოცსაკეპში გატარებული მწვანილები, ნიორი, წიწაკა და მარილი გემოვნებით.\n" +
                        "\n" +
                        "უგემრიელესი საწებელი მზად არის! გემრიელად მიირთვით!",
                "ალუჩის საწებელი",


                )
        )
        reciepList.add(
            Reciep(
                15,
                "https://static.1000.menu/img/content/14008/sous-iz-ketchupa-i-maioneza_1440276947_0_max.jpg",
                "პატარა ჯამში მოათავსეთ კეტჩუპი და მაიონეზი,დაუმატეთ წვრილად დაჭრილი ქინძი, დაჭყლეტილი ნიორი და ბოლოს მოაყარეთ მარილი,პილპილი-გემოვნებით და კარგად აურიეთ.",
                "*კეტჩე-ნეზის* სოუსი",


                )
        )


        return reciepList
    }

}