package com.example.finalexam.fragment_menu

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalexam.R
import com.example.finalexam.RecyclerView.Reciep
import com.example.finalexam.RecyclerView.RecyclerViewReciepAdapter
import com.example.finalexam.fragments_profile.MenuFragmentDirections

class DesertFragment: Fragment(R.layout.fragment_desert) {
    private lateinit var buttonBack: Button
    private lateinit var recyclerViewReciepAdapter: RecyclerViewReciepAdapter
    private lateinit var recyclerView: RecyclerView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val controller = Navigation.findNavController(view)
        buttonBack = view.findViewById(R.id.buttonBack)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerViewReciepAdapter = RecyclerViewReciepAdapter(getData())
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = recyclerViewReciepAdapter

        buttonBack.setOnClickListener() {
            val action =
                DesertFragmentDirections.actionDesertFragmentToMenuFragment()
            controller.navigate(action)
        }

    }

    private fun getData(): List<Reciep> {
        val reciepList = ArrayList<Reciep>()

        reciepList.add(
            Reciep(
                1,
                "https://gemrielia.ge/media/__sized__/images/qurabie-crop-c0-5__0-5-450x301-70.jpg",
                "კაქარს დაუმატე პუდრა, მარილი, ვანილი და ათქვიფე კარგად მიქსერით, დაუმატე სახამებელი, ფქვილი. გააერთიანე მასა და ბოლოს დაუმატე ნაღები.ცომი უნდა იყოს კრემივით (არც სქელი და არც თხელი).\n" +
                        "\n" +
                        "დასვი სასურველ ფორმებში, გულში ჩადე ჯემი და გამოაცხვე 15 წუთის განმავლობაში, 180გრადუსზე.\n" +
                        "\n" +
                        "შოკოლადი, ქოქოსი და სხვა თხილეული მოსართავად გამოიყენეთ. ",
                 "ქურაბიე",


            )
        )
        reciepList.add(
            Reciep(
                2,
                "https://gemrielia.ge/media/__sized__/images/soko-crop-c0-5__0-5-450x301.png",
                "შაქრის პუდრსა და ოთახის ტემპერატურის კაქარს თქვეფ ერთად, უმატებ კვერცხს, ვანილს და სახამებელს,ბოლოს ფქვილს-გამაფხვიერებლით და ხელით ზელავ კარგად შეკრულ ცომს.\n" +
                        "\n" +
                        "ყოფ პატარა ბურთებად (30-40გრ), დებ ფორმაზე. ყველა ბურთულას სათითაოდ აჭერ ბოთლის თავსახურს,\n" +
                        "\n" +
                        " რომელიც ყოველ ჯერზე ამოვლებული უნდა იყოს კაკაო-დარიჩინის ნაზავში. აცხობ 20-25 წუთით,180გრადუსზე.\n",
                "სოკო-ორცხობილა",


                )
        )
        reciepList.add(
            Reciep(
                3,
                "https://gemrielia.ge/media/uploads/m_shengelia3%40cu.edu.ge/2021/12/03/262856057_4945120122173213_6212857671132375623_n.jpg",
                "ფქვილი და დარბილებული ონა კრემი კარგად გააფხვიერეთ. დაამატეთ ვანილი,შაქარი და კვერცხის გული. მოზილეთ ცომი, გააკეთეთ პატარა გუნდები და ამოავსეთ ფორმა. დაჩხვლიტეთ ძირები და გამოაცხვეთ დაბალ ტემპერატურაზე.\n" +
                        "\n" +
                        "\n" +
                        "გულსართი:1 კოვზი კომშის ჯემი და ზემოდან, მოხარშული კრემი. მორთეთ საკვები დეკორით. ძალიან ხრაშუნა და გემრიელია.",
                "ტარტალეტკები",


                )
        )
        reciepList.add(
            Reciep(
                4,
                "https://gemrielia.ge/media/__sized__/images/rafaelo_GF6cDcy-crop-c0-5__0-5-450x301-70.jpg",
                "ღრმა ჯამში ქოქოსის ფანტელს ნელ-ნელა შევაზილოთ შესქელებული რძე. უნდა მივიღოთ მკვრივი მასა, ისეთი, რომ ფორმის მიცემისას არ დაიშალოს. გავაკეთოთ ბურთები ამ მასით (მე ნახევარი გამოვიყენე) და ამოვავლოთ" +
                        " გამდნარ შოკოლადში, ზემოდან ზოლებად მოასხით თეთრი შოკოლადი." +
                        "\n" +
                        "მზა გემრიელობები 1 საათით გავაჩეროთ მაცივარში, შოკოლადი რომ გამაგრდება მზადაა და შეგიძლიათ გემრიელად მიირთვათ. ",
                "ბაუნტი-1",


                )
        )
        reciepList.add(
            Reciep(
                5,
                "https://gemrielia.ge/media/__sized__/images/rafaelo_GF6cDcy-crop-c0-5__0-5-450x301-70.jpg",
                " იგივე შიგთავსი გამოვიყენოთ ზუსტად(მასის მეორე ნახევარი), ოღონდ, შუაგულში მოვათავსოთ მთლიანი თხილის გული და ისე შევკრათ ბურთი. მორჩენილი თხილი ბლენდერით დავაქუცმაცოთ, (ძალიან პუდრასავთ არ დაფქვათ," +
                        "    ცოტა საშუალო ზომებად რომ დაიტეხოს) და ჩავყაროთ გამდნარ შოკოლადში, კარგად გადავურიოთ და ისე ამოვავლოთ ბურთები." +
                        "გემრიელობები 1 საათზე მაცივარში, შოკოლადი რომ გამაგრდება მზადაა და შეგიძლიათ გემრიელად მიირთვათ. ",
                "ბაუნტი-2",


                )
        )
        reciepList.add(
            Reciep(
                6,
                "https://gemrielia.ge/media/uploads/m_shengelia3%40cu.edu.ge/2021/12/02/261398786_111677071346808_4617916464158338512_n.jpg",
                "თაფლი მიიყვანეთ ადუღებამდე, ანუ იქამდე, სანამ ბუშტუკებს წამოიღებს გვერდებიდან, შემდეგ გამორთეთ და გააცივეთ, ასე გაიმეორეთ ორჯერ.\n" +
                        "\n" +
                        "მესამედ დაამატეთ შაქარი, თხილეული და შუშეთ 10 წუთით, ნელ ცეცხლზე, ფერს რომ შეიცვლის გოზინაყი, გადმოიტანეთ წყლიან ხის დაფაზე, ცოტა რომ შეგრილდება დაჭერით, უგემრიელესია. ",
                "გოზინაყი თხილით",


                )
        )
        reciepList.add(
            Reciep(
                7,
                "https://gemrielia.ge/media/__sized__/images/shokoladi_rV5hmSs-crop-c0-5__0-5-450x301-70.jpg",
                "4ს.კ. კაკაო, 4 ს.კ.შაქარი და 8 ს.კ. რძე აურიეთ. დადგით დაბალ ცეცხლზე, მიიყვანეთ ადუღებამდე და გადმოდგით. ცივი 50 გრ.კარაქი ცხელშივე " +
                        "დაუმატეთ, მორევით გაადნეთ. შემდეგ კი მასში 50 გრ.შოკოლადიც გაალღვეთ. " +
                        "მორევისას თანდათან სქელდება. ჩაასხით ქილაში და გემრიელად მიირთვით. ",
                "შოკოლადის პასტა",


                )
        )
        reciepList.add(
            Reciep(
                8,
                "https://gemrielia.ge/media/__sized__/images/261644034_1651652045221028_3055027173931991607_n-crop-c0-5__0-5-450x301-70.jpg",
                "მანდარინის წვენს დაუმატეთ ჟელატინი ( გამოყენებულია სწრაფმოქმედი ), გააჩერეთ 10 წუთით და შემოდგით ცეცხლზე. დაუმატეთ 200გრ შაქარი, მიიყვანეთ ადუღებამდე ( შაქარი და ჟელატინი კარგად უნდა გაიხსნას, არ უნდა ადუღდეს მასა ).\n" +
                        "\n" +
                        "ჩაასხით ფორმაში და მთელი ღამით გააჩერეთ მაცივარში.",
                "მანდარინის ჟელე",


                )
        )
        reciepList.add(
            Reciep(
                9,
                "https://gemrielia.ge/media/__sized__/images/deserti_ufoyBZF-crop-c0-5__0-5-450x301-70.jpg",
                "პირველ რიგში, ჟელატინი დაალბეთ რძეში, ან წყალში. ბანანი, არაჟანი, შესქ.რძე დააბლენდერეთ, ბოლოს ჟელატინი ორთქლის აბაზანაზე, ან მიკროტალღურ ღუმელში გაალღვეთ. შეურიეთ არაჟნის მასას, გადაანაწილეთ ჭიქებში და მაცივარში მოათავსეთ " +
                        "დაახლოებით 2 საათით, უგემრიელესია. ",
                "იოგურტ-დესერტი",


                )
        )
        reciepList.add(
            Reciep(
                10,
                "https://gemrielia.ge/media/__sized__/images/gozinayi_geLwsrW-crop-c0-5__0-5-450x301-70.jpg",
                "თაფლი მიიყვანეთ ადუღებამდე ორჯერ და გააცივეთ, მესამედ დაუმატეთ შაქარი და სეზამი ნელ ცეცხლზე შუშეთ, 3–4 წუთით, თან ურიეთ, გადმოიღეთ წყლიან ხის დაფაზე, გააბრტყელეთ, ცოტა რომ შეგრილდება, დაჭერით.",
                "თეთრი სეზამი",


                )
        )
        reciepList.add(
            Reciep(
                11,
                "https://gemrielia.ge/media/__sized__/images/saaxalwlo_nadzvis_xe-crop-c0-5__0-5-450x301-70.jpg",
                "ცილა ათქვიფეთ მარილთან ერთად, შეურიეთ გული და შაქარი. ნუში გახეხეთ, შეურიეთ სახამებელი, ფქვილი და შეურიეთ ორივე ნარევი. დაალაგეთ ცომი ქაღალდის კონუსებში და გამოაცხვეთ ღუმელში, 15-20 წუთის განმავლობაში, 180 გრადუსზე. შოკოლადი გაადნეთ " +
                        "წყლის აბაზანაზე და გამომცხვარ „ნაძვის ხეებს“ ფუნჯით წაუსვით, გააბრტყელეთ დაჭრილი ფისტა. სუფრაზე მიტანის წინ ნამცხვრებს მოაყარეთ შაქრის პუდრა.",
                "ნაძვი-ნამცხვარი",


                )
        )
        reciepList.add(
            Reciep(
                12,
                "https://gemrielia.ge/media/__sized__/images/blitebi_fnad4LZ-crop-c0-5__0-5-450x301-70.jpg",
                "კვერცხი და შაქარი ათქვიფეთ კარგად , შემდეგ დაამატეთ ხაჭო, გამაფხვიერებელი, ვანილი და ფქვილი კარგი მორევით მიიყვანეთ ერთგვარ მასამდე და დააყოვნეთ 10-15 წუთით.\n" +
                        "\n" +
                        "ამასობაში, ქვაბში ჩაასხით ზეთი, დადგით გაზქურაზე და ცხელ ზეთში ჩაყარეთ პატარა ბურთის ფორმებად.\n" +
                        "\n" +
                        "ტემპერატურა აკონტროლეთ, ძალიან არ გაცხელდეს ზეთი. ",
                "ხაჭოს ბლითი",


                )
        )
        reciepList.add(
            Reciep(
                13,
                "https://gemrielia.ge/media/__sized__/images/vafli-crop-c0-5__0-5-450x301-70.jpg",
                "კვერცხი, ვანილი და შაქარი ათქვიფეთ მიქსერით კარგად, დაამატეთ გამდნარი მარგარინი ონა და დაამატეთ ფქვილი. საცხობ ფირმაში, შუაში, 1 კოვზი ცომი დაასხით და გამოაცხვეთ. ცხელივე გადაახვიეთ სასურველ ფორმაზე.",
                "ვაფლი",


                )
        )
        reciepList.add(
            Reciep(
                14,
                "https://gemrielia.ge/media/__sized__/images/167819942_279452790311456_8481608833529408901_n-crop-c0-5__0-5-450x301-70.jpg",
                "ფქვილში ავფშვნიტეთ კარაქი, დაამატეთ არაჟანი, ვანილი, მარილი, 1ცალი კვერცხი. მოზილეთ ერთგვაროვანი მასა და შეინახეთ მაცივარში, 40 წუთის განმავლობაში.\n" +
                        "\n" +
                        "გაატარეთ ნიგოზი და აურიეთ ქიშმიშში, დაამატეთ ლიმონის ცედრა, ასევე შეგიძლიათ 2 ცალი კვერცხის დამატებაც. გააბტყელეთ ცომი თხლად, გაშალეთ გულსართი თანაბრად და გადაახვიეთ. ზემოდან წაუსვით კვერცხის გული და შედგით ღუმელში. ",
                "ნიგვზიანი რულეტი",


                )
        )
        reciepList.add(
            Reciep(
                15,
                "https://gemrielia.ge/media/__sized__/images/eklerebi-crop-c0-5__0-5-450x301.png",
                "წყალი, არაყი, მარგარინი, ცოტა მარილი და შაქარი მივიყვანოთ ადუღებამდე. ჩავაყაროთ 200-გრამიანი ჭიქით ფქვილი. დავუმატოთ 1 ჩკ კაკაო და ყავა ერთად (ანუ ნახევარი ჩკ კაკაო და ნახევარი ჩკ ხსნადი ყავა). ვშუშოთ დაბალ ცეცხლზე 5 წუთით და რომ შეგრილდება, " +
                        "დავამატოთ 4 დიდი ზომის კვერცხი. გამოვაცხოთ 180 გრადუსზე ზედა-ქვედა წვაზე, 45 წუთით. გავაცივოთ ღუმელშივე. ",
                "ეკლერები",


                )
        )


        return reciepList
    }

}