package com.example.finalexam.fragment_menu

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalexam.R
import com.example.finalexam.RecyclerView.Reciep
import com.example.finalexam.RecyclerView.RecyclerViewReciepAdapter
import com.example.finalexam.fragments_profile.MenuFragmentDirections

class SaladFragment: Fragment(R.layout.fragment_salad) {
    private lateinit var buttonBack: Button
    private lateinit var recyclerViewReciepAdapter: RecyclerViewReciepAdapter
    private lateinit var recyclerView: RecyclerView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val controller = Navigation.findNavController(view)
        buttonBack = view.findViewById(R.id.buttonBack)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerViewReciepAdapter = RecyclerViewReciepAdapter(getData())
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = recyclerViewReciepAdapter

        buttonBack.setOnClickListener() {
            val action =
                SaladFragmentDirections.actionSaladFragmentToMenuFragment()
            controller.navigate(action)
        }

    }

    private fun getData(): List<Reciep> {
        val reciepList = ArrayList<Reciep>()

        reciepList.add(
            Reciep(
                1,
                "https://gemrielia.ge/media/__sized__/images/sazamtrossalata-crop-c0-5__0-5-450x301-70.jpg",
                "საზამთრო დაჭერით კუბებად, დაუმატეთ ასევე დაჭრილი ყველი, ზეთისხილი, ბაზილიკი. მოასხით სოუსი, ამოურიეთ და მიირთვით. ",
                 "საზამთრო ყველით",


            )
        )
        reciepList.add(
            Reciep(
                2,
                "https://gemrielia.ge/media/__sized__/images/salata_L8EcdeH-crop-c0-5__0-5-450x301-70.jpg",
                "გახეხეთ კომბოსტო, სტაფილო, დაჭერით წიწაკა და დაასხით ცხელი მარინადი: 1 ლიტრა წყალი, ძმარი, მარილი, შაქარი, დაფნის ფოთოლი," +
                        " შავი პილპილის კაკლები - ეს ყველაფერი ადუღეთ 2-3 წუთით. ზემოდან სიმძიმე დაადეთ და შეინახეთ გრილ ადგილას. მესამე დღეს შეგიძლიათ გემრიელად ახრაშუნოთ. ",
                "მჟავე კომბოსტო",


                )
        )
        reciepList.add(
            Reciep(
                3,
                "https://gemrielia.ge/media/__sized__/images/saaxalwlo_burtulebi-crop-c0-5__0-5-450x301-70.jpg",
                "ყველა ინგრედიენტი აურიეთ ერთგვაროვნებამდე, მიეცით ბურთულების ფორმა და მოაყარეთ კამა, პაპრიკა, ან სეზამის მარცვლები. თითოეულ ბურთზე მოათავსეთ ზეთისხილი, მოაყარეთ კამა, ან ოხრახუშის ყლორტები. ",
                "საახალწლო ბურთულები",


                )
        )
        reciepList.add(
            Reciep(
                4,
                "https://gemrielia.ge/media/__sized__/images/badrijnis_salata_TON85lo-crop-c0-5__0-5-450x301-70.jpg",
                "ბადრიჯანი და ყაბაყი დავჭრათ თხელ ნაჭრებად. წიწაკები გავყოთ შუაზე და თესლი გამოვაცალოთ. ხახვი და რეჰანი წვრილად დავკეპოთ, ნიორი დავნაყოთ. დაჭრილ ბოსტნეულს მოვაყაროთ მარილი, მოვასხათ ზეითუნის " +
                        "ზეთი და გრილზე შევწვათ. შემწვარი ბულგარული წიწაკები სასურველ ზომაზე დავჭრათ, ცხარე წიწაკა დავკეპოთ. ყველა ინგრედიენტი ავურიოთ და შევაზავოთ ძმრით.",
                "ბადრიჯანის სალათი",


                )
        )
        reciepList.add(
            Reciep(
                5,
                "https://gemrielia.ge/media/__sized__/images/qatmis_salata_h444HPB-crop-c0-5__0-5-450x301-70.jpg",
                "ფილე მოხარშეთ, დაანაწევრეთ, დაჭერით ბულგარული წიწაკა. დაამატეთ სიმინდი, მოხარშული ბრინჯი, მაიონეზი, ქინძი, კამა, წიწაკა და მარილი. გემრიელად მიირთვით. ",
                "ქათმის სალათი",


                )
        )
        reciepList.add(
            Reciep(
                6,
                "https://gemrielia.ge/media/__sized__/images/shuba_l8bDcai-crop-c0-5__0-5-450x301-70.jpg",
                "მოხარშთ კვერცხი, კარტოფილი და ჭარხალი. გახეხეთ წვრილად ყველა ინგრედიენტი და დაიწყეთ ფენა–ფენა სალათის აწყობა. \n" +
                        "\n" +
                        "პირველ ფენა არის კომბოსტო, შემდეგ კარტოფილი, მარილი და პილპილი გემოვნებით, მწვანე ხახვი, მაიონეზი, სტაფილო, ქინძი, მაიონეზი, ჩაახეხეთ მოხარშული კვერცხი. მოაყარეთ გახეხილი ჭარხალი და სალათაც მზად არის.\n"+"" +
                        " გემრიელად მიირთვით. ",
                "სალათი *შუბა*",


                )
        )
        reciepList.add(
            Reciep(
                7,
                "https://gemrielia.ge/media/__sized__/images/shuba_4KCuqpP-crop-c0-5__0-5-450x301-70.jpg",
                "მოხარშეთ ბოსტნეული და გახეხეთ თანმიმდევრობით. ამ შემთხვევაში გამოყენებულია ეს თანმიმდევრობა:\n" +
                        "\n" +
                        "კარტოფილი, სტაფილო, ჭარხალი, მაიონეზი, ბოლოკი, კიტრი, კარტოფილი, მაიონეზი, სტაფილო, ჭარხალი, მაიონეზი. ფენებზე მოაყარეთ მარილი. ზემოდან მორთეთ დაჭრილი ბოლოკით,კიტრით და მწვანილით",
                "სამარხვო *შუბა*",


                )
        )
        reciepList.add(
            Reciep(
                8,
                "https://gemrielia.ge/media/__sized__/images/qatmis_salata_vbUs4u5-crop-c0-5__0-5-450x301-70.jpg",
                "მოხარშეთ ქათმის ფილე, აჩეჩეთ ჩანგლის დახმარებით. დაამატეთ წვრილად, გრძლად დაჭრილი ხახვი, ბულგარული წიწაკა, გახეხილი უმი სტაფილო, კამა, მარილი, პილპილი. ხარი გააფორმეთ კვერცხის ცილით, კვერცხის გულით, ზეითუნის ხილით, ლიმონის ქერქითა და პურით.",
                "ქათმის სალათი",


                )
        )
        reciepList.add(
            Reciep(
                9,
                "https://gemrielia.ge/media/__sized__/images/charxlis_salata-crop-c0-5__0-5-450x301-70.jpg",
                "გახეხეთ მოხარშული ჭარხალი და კვერცხი. დაუმატეთ წვრილად დაჭრილი მჟავე კიტრი.\n" +
                        "\n" +
                        "დააბლენდერეთ რძე და მცენარეული ზეთი. დაუმატეთ მწიკვი მარილი, ლიმონის წვენი, ნიორი გემოვნებით და მდოგვი. გააგრძელეთ დაბლენდერება და სოუსიც მზადაა. დაუმატეთ სალათას და კარგად მოურიეთ. \n" +
                        "\n" +
                        "გემრიელად მიირთვით!",
                "ჭარხლის სალათი",


                )
        )
        reciepList.add(
            Reciep(
                10,
                "https://gemrielia.ge/media/__sized__/images/bf52a163-crop-c0-5__0-5-450x301-70.jpg",
                "დაჭერით ხახვი წვრილ რგოლებად და შეწვით ზეთში და მარილში, სანამ არ მიიღებს ოქროსფერ შეფერილობას და დაამატეთ მდოგვი.\n" +
                        "დაჭერით კომბოსტო ძალიან წვრილად და დაამატეთ შემწვარი ხახვის მასას.\n" +
                        " დაამატეთ: დაჭრილი მწვანე ხახვი, ნიგოზი, მარილი და წიწაკა გემოვნებით.",
                "კომბოსტოს სალათი",


                )
        )
        reciepList.add(
            Reciep(
                11,
                "https://gemrielia.ge/media/__sized__/images/mwvanepamidvrissalata-crop-c0-5__0-5-450x301-70.jpg",
                "ყველა ინგრედიენტი დაჭერით, შეაზავეთ სუნელებითა და მწვანილებით. მოასხით ზეთი და ძმარი და კარგად ამოურიეთ. გააჩერეთ რამდენიმე საათით, რომ არომატები კარგად გაუჯდეს. შემდეგ ქილებში გადაანაწილეთ და გაასტერილეთ. ",
                "მწვანე პამიდრის სალათი",


                )
        )
        reciepList.add(
            Reciep(
                12,
                "https://gemrielia.ge/media/__sized__/images/salata_BBnWMui-crop-c0-5__0-5-450x301-70.jpg",
                "სოკო ზეთში მოთუშეთ (ან მზა, კონსერვის გამოიყენეთ). მარილი და პილპილი დაუმატეთ.\n" +
                        "\n" +
                        "მოხარშული კვერცხი, ხახვი, კიტრი, კამა, მწვანე ხახვი დაჭერით. \n" +
                        "\n" +
                        "ყველა ინგრედიენტი შეურიეთ ერთმანეთს და შეაზავეთ მაიონეზით, არაჟნითა და წვრილად დაჭრილი ნივრით. კარგად ამოურიეთ და მიირთვით.",
                "სოკოს სალათი",


                )
        )
        reciepList.add(
            Reciep(
                13,
                "https://gemrielia.ge/media/__sized__/images/7f6a77cefe6a1f02d15d80f8a8f47fa3-crop-c0-5__0-5-450x301-70.jpg",
                "სალათის მომზადება დაიწყეთ მოხარშული ფილეს დაჭრით. დაჭერით საშუალო ნაჭრებად და დაუმატეთ გახეხილი სტაფილო, კუბებად დაჭრილი ბულგარული წიწაკა და ვაშლი და მწვანილები. ზემოდან მოასხით მაიონეზი და მდოგვის მარცვლებისა და არატკბილი იოგურტის ნაზავი!\n" +
                        "\n" +
                        "სულ ეს არის, გემრიელად მიირთვით!",
                "ქათმის სალათი",


                )
        )
        reciepList.add(
            Reciep(
                14,
                "https://gemrielia.ge/media/__sized__/images/salata_leLbPm6-crop-c0-5__0-5-450x301-70.jpg",
                "ალბათ, ეს ერთ-ერთი ყველაზე მარტივი სალათაა, რისი გაკეთებაც კი ზაფხულში შეგვიძლია. ყველა ინგრედიენტი დაჭერით, ლიმონის წვენითა და სუნელებით შეაზავეთ და სულ ეს არის!",
                "საზაფხულო სალათი",


                )
        )
        reciepList.add(
            Reciep(
                15,
                "https://gemrielia.ge/media/__sized__/images/salata_Ws2cNOe-crop-c0-5__0-5-450x301-70.jpg",
                "ქათმის ფილე დაჭერით, მარილი და პილპილი მოაყარეთ და ცოტა ზეთში შეწვით." +
                        "ლორი და პამიდორი დაჭერით." +
                        "ქათმის ფილეს დაუმატეთ ლორი და ერთად შეწვით რამდენიმე წუთით." +
                        "სოუსის ყველა ინგრედიენეტი შეურიეთ ერთმანეთს. " +
                        "ბოლოს სალათის ინგრედიენტები ერთ დიდ ჯამში მოათავსეთ, თავზე სოუსი მოასხით.\n" +
                        "გემრიელად მიირთვით!",
                "სალათა *იმპერატორი*",


                )
        )


        return reciepList
    }

}