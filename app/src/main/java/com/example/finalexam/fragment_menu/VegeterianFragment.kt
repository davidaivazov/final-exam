package com.example.finalexam.fragment_menu

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalexam.R
import com.example.finalexam.RecyclerView.Reciep
import com.example.finalexam.RecyclerView.RecyclerViewReciepAdapter
import com.example.finalexam.fragments_profile.MenuFragmentDirections

class VegeterianFragment: Fragment(R.layout.fragment_vegeterian) {
    private lateinit var buttonBack: Button
    private lateinit var recyclerViewReciepAdapter: RecyclerViewReciepAdapter
    private lateinit var recyclerView: RecyclerView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val controller = Navigation.findNavController(view)
        buttonBack = view.findViewById(R.id.buttonBack)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerViewReciepAdapter = RecyclerViewReciepAdapter(getData())
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = recyclerViewReciepAdapter

        buttonBack.setOnClickListener() {
            val action =
                VegeterianFragmentDirections.actionVegeterianFragmentToMenuFragment()
            controller.navigate(action)
        }

    }

    private fun getData(): List<Reciep> {
        val reciepList = ArrayList<Reciep>()

        reciepList.add(
            Reciep(
                1,
                "https://gemrielia.ge/media/__sized__/images/brokkoli-crop-c0-5__0-5-450x301-70.jpg",
                "მოხარშეთ კომბოსტო და ბროკოლი. დაბალ ცეცხლზე დადგით რძე და დაუმატეთ ფქვილი, პარმეზანი, კარაქი, მუსკატის კაკალი და მარილი. მოათავსეთ ბროკოლი ფართო თეფშზე და მიეცით ნაძვის ხის ფორმა.",
                 "ნაძვის-სალათი",


            )
        )
        reciepList.add(
            Reciep(
                2,
                "https://gemrielia.ge/media/__sized__/images/gufta_dd9sKSt-crop-c0-5__0-5-450x301-70.jpg",
                "ბადრიჯანი დაჭერით სულ თხლად, მოაყარეთ მარილი, გააჩერეთ 20 წუთი, გარეცხეთ ცივი წყლით და ოდნავ შეწვით. \n" +
                        "დაჭერით დანარჩენი ბოსტნეული და შეახვიეთ, მიეცით რულეტის ფორმა.\n" +
                        "ქილები გაასტერილეთ, ძირში ჩადეთ ნიორი და პილპილის მარცვლები, ჩაალაგეთ ბადრიჯანი. მოასხით მარინადი. ბოლოს კი აავსეთ ქილა მდუღარე წყლით გააჩერეთ 5 წუთი და მერე გაასტერილეთ" +
                        " 30 წუთი ადუღებიდან, მოხუფეთ. დილამდე შეინახეთ თბილად.",
                "გუფთა",


                )
        )
        reciepList.add(
            Reciep(
                3,
                "https://gemrielia.ge/media/images/146167173_4226941280666741_1056930639882762949_n.jpg",
                "ცალკე მოხარშეთ ხორცი, (რადგანაც დიდი დრო სჭირდება). მოხრაკეთ ხახვი, დაამატეთ პაპრიკა, ხასხასა ფერი რომ წამოვა, დაამატეთ დაჭრილი პომიდორი, სტაფილო, კარტოფილი, ხორცი, ბადრიჯანი, ბულგარული წიწაკა, ხორცის ბულიონი იმდენი, რომ არ გათხელდეს სადილი.\n" +
                        "\n" +
                        "მოხარშეთ, ბოლოს მწვანილი, წიწაკა, ნიორი და მარილი დაამატეთ. გემრიელად მიირთვით. ",
                "ბადრიჯანი",


                )
        )
        reciepList.add(
            Reciep(
                4,
                "https://gemrielia.ge/media/__sized__/images/pamidori-crop-c0-5__0-5-450x301-70.jpg",
                "კამა, ტარხუნა და ნიორი დაჭერით. გადაიტანეთ ჯამში. დაუმატეთ შაქარი, მარილი, წითელი და შავი პილპილი. კარგად ამოურიეთ ან დანაყეთ.\n" +
                        "პამიდორი გასერეთ ოთხ ნაწილად (ისე, რომ ნაწილები არ დაშალოს, ცენტრზე ოდნავ დაბლა) და მწვანილების შიგთავსი ჩაუდეთ. პამიდვრები მოათავსეთ პარკში ან კონტინერში, კარგად შეანჯღრიეთ და რამდენიმე საათით დააყოვნეთ, რომ გემოები კარგად გაუჯდეს.\n" +
                        "\n" +
                        "გემრიელად მიირთვით!",
                "პამიდორი მწვანილებით",


                )
        )
        reciepList.add(
            Reciep(
                5,
                "https://gemrielia.ge/media/__sized__/images/kartofili_Vl7sfGy-crop-c0-5__0-5-450x301-70.jpg",
                "გაასუფთავეთ კარტოფილი და მოხარშეთ მარილწყალში. გააციეთ და შემდეგ დაჭერით.\n" +
                        "დაჭერით ბეკონი წვრილად, შეწვით და დაუმატეთ წვრილად დაჭრილი ხახვი. \n" +
                        "მოათავსეთ კარტოფილი ტაფაზე, მოურიეთ კარგად, მოამზადეთ საშუალო ცეცხლზე,სანამ არ დაიბრაწება. მომზადების დასრულებამდე,მოაყარეთ ბეკონი, მარილი და შავი პილპილი.\n" +
                        "გადმოდგით ტაფიდან და გემრიელად მიირთვით!\n",
                "კარტოფილი სოკოსთან",


                )
        )
        reciepList.add(
            Reciep(
                6,
                "https://gemrielia.ge/media/__sized__/images/badrijani_e0rjDsf-crop-c0-5__0-5-450x301-70.jpg",
                "წვრილად დაჭრილი ხახვი მოხრაკეთ მცენარეულ ზეთში. გათლილი ბადრიჯანი დაჭერით მრგვალ ნაჭრებად, მოათავსეთ ქვაბში , დაასხვით მცენარეული ზეთი და ოდნავ მოხრაკეთ, შემდეგ " +
                        " ჩააყარეთ მოხრაკული ხახვი, პილპილი, წვრილად დაჭრილი ოხრახუში,მცენარეული ზეთი, მარილი, დაასხით ნახევარი ჩაის ჭიქა წყალი, ქვაბს დაახურეთ.  მაწონს შეურიეთ დანაყილი " +
                        "ნიორი და კარგად ურიეთ. დაამატეთ ბადრიჯანს და გემრიელად მიირთვით!",
                "ბადრიჯანი მაწონში",


                )
        )
        reciepList.add(
            Reciep(
                7,
                "https://gemrielia.ge/media/__sized__/images/thumb-840x440_y4l15wv-crop-c0-5__0-5-450x301-70.jpg",
                "გაასუფთავეთ კარტოფილი და მოხარშეთ მარილწყალში. გააციეთ და შემდეგ დაჭერით.\n" +
                        "დაჭერით ბეკონი წვრილად, შეწვით და დაუმატეთ წვრილად დაჭრილი ხახვი. \n" +
                        "მოათავსეთ კარტოფილი ტაფაზე, მოურიეთ კარგად, მოამზადეთ საშუალო ცეცხლზე,სანამ არ დაიბრაწება. მომზადების დასრულებამდე,მოაყარეთ ბეკონი, მარილი და შავი პილპილი.\n" +
                        "გადმოდგით ტაფიდან და გემრიელად მიირთვით!\n",
                "ახალი კარტოფილი",


                )
        )
        reciepList.add(
            Reciep(
                8,
                "https://gemrielia.ge/media/__sized__/images/mobracula_3K3pkHA-crop-c0-5__0-5-450x301-70.jpg",
                "ნაღები, კვერცხი, ნივრის ფხვნილი, პილპილი, მარილი და მუსკატის კაკალი მსუბუქად ავთქვიფოთ და გადავდოთ გვერძე. ყველი გავხეხოთ. გასუფთავებული კარტოფილი და სტაფილო გავხეხოთ ზალიან თხელ ფირფიტებად. საცხობ ფორმაში ჩავდოთ\n" +
                        "ერთ ფენად ცოტა კარტოფილი, შემდეგ სტაფილო, შემდეგ ყველი და მოვასხათ წინასწარ მომზადებული ნაღების სოუსი.\n" +
                        "ასე გავიმეორებთ ყველა ფენას. ზემოდან გადავაკრათ ფოლგა და გამოვაცხოთ 190 გრადუსზე დაახლოებით 2 საათი.",
                "მობრაწულა  ",


                )
        )
        reciepList.add(
            Reciep(
                9,
                "https://gemrielia.ge/media/__sized__/images/მწვანეპომიდორი-crop-c0-5__0-5-450x301-70.jpg",
                "დაჭერით პომიდორი 1 სმ სისქის ნაჭრებად. ათქვიფეთ კვერცხი რძესთან ერთად. ცალკე ჯამში ერთმანეთში აურიეთ მჭადის ფქვილი (ან საფანელი), მარილი და პილპილი. \n" +
                        "გაადნეთ ფართე ტაფაზე კარაქი, ამოავლეთ პომიდვრის ნაჭრები ჯერ კვერცხის, შემდეგ ფქვილის მასაში და შეწვით ყველა მხრიდან 2-2 წუთს განმავლობაში.\n" +
                        "\n" +
                        "გემრიელად მიირთვით!",
                "მკვახე პომიდორი",


                )
        )
        reciepList.add(
            Reciep(
                10,
                "https://gemrielia.ge/media/__sized__/images/simindi1_qmAUMFw-crop-c0-5__0-5-450x301-70.jpg",
                "ღრმა ჯამში მოათავსეთ რბილი კარაქი, აუმატეთ ჩილის სოუსი, ლიმონის წვენი, პროვანსული ბალახები, სოიოს სოუსი და არაჟანი. ყველაფერი კარგად აურიეთ.\n" +
                        "სილიკონის ფუნჯის მეშვეობით წაუსვით მიღებული მასა გარჩეულ სიმინდს. გაახვიეთ თითოეული სიმინდი ფოლგაში, მოათავსეთ ღუმელის ფირფიტაზე და აცხვეთ 220 გრადუსზე 20 წუთის განმავლობაში.\n" +
                        "\n" +
                        "გემრიელად მიირთვით!",
                "გამომცხვარი სიმინდი",


                )
        )
        reciepList.add(
            Reciep(
                11,
                "https://gemrielia.ge/media/__sized__/images/ბადრიჯანიმარტივად-crop-c0-5__0-5-450x301-70.jpg",
                "ბადრიჯანი გავფცქვნათ, წვრილ ზოლებად დავჭრათ და ზეთში შევწვათ, დავუმატოთ დანაყილი ნიორი, ქინძი, წიწაკა, მარილი და ძმარი.\n",
                "ბადრიჯანი წიწაკით",


                )
        )
        reciepList.add(
            Reciep(
                12,
                "https://gemrielia.ge/media/__sized__/images/kartofili_4NsOVqR-crop-c0-5__0-5-450x301-70.jpg",
                "ხახვი დავჭრათ ალყა-ალყა, მწვანილი – წვრილად, ხოლო კარტოფილი – რგოლებად. ქოთნის ძირში ჩავდოთ 1 ს.კ. კარაქი, ზედ დავაწყოთ კარტოფილი, " +
                        "ხახვი, მწვანილი და ტყემალი. მოვაყაროთ მარილი და წიწაკა. ზემოდან მეორე ფენა ამავე მეთოდით დავაწყოთ, ბოლოს დავადოთ კვლავ 1 ს.კ. კარაქი, " +
                        "დავასხათ იმდენი წყალი, რომ კარტოფილი არ დაიფაროს, დავახუროთ სახურავი და მაღალ ტემპერატურაზე გახურებულ ღუმელში შევდგათ, სანამ კარგად მოიხარშება.",
                "კარტოფილი ტყემლით",


                )
        )
        reciepList.add(
            Reciep(
                13,
                "https://gemrielia.ge/media/__sized__/images/gamomcxvarikartofili1-crop-c0-5__0-5-450x301-70.jpg",
                "კარტოფილი გავფცქვნათ და მსხვილ ნაჭრებად დავჭრათ, მოვაყაროთ მარილი, პილპილი, როზმარინი და შევაზილოთ\n" +
                        "ცოტაოდენი ზეთი. კარტოფილი მოვათავსოთ ფორმაში და 200 გრადუსზე გახურებულ ღუმელში გამოვაცხოთ, სანამ შეიბრაწება.\n" +
                        "მზა კერძს მოვაყაროთ წვრილად დაჭრილი მწვანე ხახვი.",
                "გამომცხვარი კარტოფილი",


                )
        )
        reciepList.add(
            Reciep(
                14,
                "https://gemrielia.ge/media/__sized__/images/kabakinigvzit4-crop-c0-5__0-5-450x301-70.jpg",
                "ყაბაყი გრძელ თხელ ფენებად დავჭრათ და შევწვათ ზეთში. ამავე ზეთში მოვშუშოთ წვრილად დაჭრილი ხახვი. ნიგოზი გავატაროთ, დავუმატოთ სანელებლები, მწვანილი, ძმარი და ხახვი. ნიგვზის მასა წავუსვათ შემწვარ ბოსტნეულს და გადავახვიოთ რულეტივით.\n" +
                        "რჩევა:\n" +
                        "შეიძლება გამოვიყენოთ უმი ხახვიც, ოღონდ, ძალიან წვრილად უნდა დავჭრათ.",
                "ყაბაყი ნიგვზით",


                )
        )
        reciepList.add(
            Reciep(
                15,
                "https://gemrielia.ge/media/__sized__/images/briuseliskombosto_ugYde7P-crop-c0-5__0-5-450x301-70.jpg",
                "გარეცხილი და გასუფთავებული ბრიუსელის კომბოსტო და კუბებად დაჭრილ გოგრა მოვათავსოთ ღუმლის ტაფაზე, დავუმატოთ მარილი, ზეითუნის ზეთი და" +
                        " შევდგათ ღუმელში დაახლოებით 25 წთ. დავუმატოთ მოხალული ნუში, მშრალი კენკრა და ნეკერჩხლის სიროფი.",
                "ბრიუსელის კომბოსტო",


                )
        )


        return reciepList
    }

}