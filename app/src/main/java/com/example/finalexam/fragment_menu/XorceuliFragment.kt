package com.example.finalexam.fragment_menu

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalexam.R
import com.example.finalexam.RecyclerView.Reciep
import com.example.finalexam.RecyclerView.RecyclerViewReciepAdapter
import com.example.finalexam.fragments_profile.MenuFragmentDirections

class XorceuliFragment: Fragment(R.layout.fragment_xorceuli) {
    private lateinit var buttonBack: Button
    private lateinit var recyclerViewReciepAdapter: RecyclerViewReciepAdapter
    private lateinit var recyclerView: RecyclerView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val controller = Navigation.findNavController(view)
        buttonBack = view.findViewById(R.id.buttonBack)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerViewReciepAdapter = RecyclerViewReciepAdapter(getData())
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = recyclerViewReciepAdapter

        buttonBack.setOnClickListener() {
            val action =
                XorceuliFragmentDirections.actionXorceuliFragmentToMenuFragment()
            controller.navigate(action)
        }

    }

    private fun getData(): List<Reciep> {
        val reciepList = ArrayList<Reciep>()

        reciepList.add(
            Reciep(
                1,
                "https://gemrielia.ge/media/__sized__/images/263169975_114267837754398_8060807936313934041_n-crop-c0-5__0-5-450x301-70.jpg",
                "გასუფთავებულ ღორის თავ–ფეხს დაამატეთ დაფნა, ხახვი, წყალი იმდენი, რომ კარგად დაიფაროს. კარგად რომ მოიხარშება, ამოიღეთ ხორცი, ძვლები მოაცალეთ და დაკეპეთ წვრილად. ნახარში გააჩერეთ 10 წუთით და გადაწურეთ საცერში. \n" +
                        "ხორცს დაასხით იმდენი ნახარში, რომ დაიფაროს ხორცი. დაამატეთ გატარებული ნიორი, მარილი, შავი პილპილის მარცვლები, წითელი დაფქული წიწაკა, კარგად აურიეთ და გადაანაწილეთ მინის ჭურჭელში\n" +
                        " მოხარშული სტაფილოსთან, კამასა და ლიმონთან ერთად.",
                 "ხალადეცი",


            )
        )
        reciepList.add(
            Reciep(
                2,
                "https://gemrielia.ge/media/__sized__/images/gufta_dd9sKSt-crop-c0-5__0-5-450x301-70.jpg",
                "საქონლის ფარშს დაუმატეთ ბრინჯი, წიწაკა, ქინძი, ოხრახუში, გატარებული ხახვი, კვერცხი,ნიორი და აურიეთ. გამზადეთ პატარა ზომის გუფთები.\n" +
                        "\n" +
                        "დაჭერით ერთი თავი ხახვი რგოლებად მოთუშეთ ზეთში დამტეთ ტომატი და დასხით ადუღებული წყალი ჩააწყვეთ გუფთები. წყალი უნდა იყოს ქვაბში იმდენი, რომ გუფთებს ნახევრად ფარავდეს. მოხარშეთ და გემრიელად მიირთვით.",
                "გუფთა",


                )
        )
        reciepList.add(
            Reciep(
                3,
                "https://gemrielia.ge/media/__sized__/images/xorceuli-crop-c0-5__0-5-450x301-70.jpg",
                "ცალკე მოხარშეთ ხორცი, (რადგანაც დიდი დრო სჭირდება). მოხრაკეთ ხახვი, დაამატეთ პაპრიკა, ხასხასა ფერი რომ წამოვა, დაამატეთ დაჭრილი პომიდორი, სტაფილო, კარტოფილი, ხორცი, ბადრიჯანი, ბულგარული წიწაკა, ხორცის ბულიონი იმდენი, რომ არ გათხელდეს სადილი.\n" +
                        "\n" +
                        "მოხარშეთ, ბოლოს მწვანილი, წიწაკა, ნიორი და მარილი დაამატეთ. გემრიელად მიირთვით. ",
                "ოჯახური კერძი",


                )
        )
        reciepList.add(
            Reciep(
                4,
                "https://gemrielia.ge/media/__sized__/images/qatamipomidorshi-crop-c0-5__0-5-450x301-70.jpg",
                "ქათმის ხორცი დავჭრათ ნაჭრებად, მოვაყაროთ მარილი და 200 გრადუსზე გახურებულ ღუმელში შევბრაწოთ. პარალელურად დავმდუღროთ პამიდორი, გავაცალოთ კანი და ბლენდერში დავაქუცმაცოთ. ხახვი წვრილად დავჭრათ და კარაქში მოვშუშოთ. დავუმატოთ პამიდორი და თავდახურული," +
                        " დაბალ ცეცხლზე ვშუშოთ, მოვაყაროთ მარილი. შემწვარი ქათამი დავჭრათ, ჩავაწყოთ სოუსში და წამოვადუღოთ, მოვაყაროთ დაჭრილი მწვანილი, დანაყილი ნიორი და წიწაკა.",
                "ქათამი პამიდვრის სოუსში",


                )
        )
        reciepList.add(
            Reciep(
                5,
                "https://gemrielia.ge/media/__sized__/images/goris_xorcis_ostri-crop-c0-5__0-5-450x301-70.jpg",
                "ხორცი დაჭერით სასურველ ზომებად და ჩაყარეთ ქვაბში, მოშუშეთ. დაუმატეთ ზეთი და წვრილად დაჭრილი ხახვი, დაელოდეთ ხორცის მოხარშვას. უკვე მომზადებულ ხორცს დაუმატეთ ნიორი, ტომატი და პომიდვრის საწებელი.\n" +
                        "\n" +
                        "დააყოვნეთ 5 წუთით და 200 გრ მდუღარე წყალი დაასხით, შესათხელებლად შეაგემოვნეთ მწვანილებით, დაუმატეთ პაპრიკა და მარილი გემოვნებით. ",
                "ღორის ოსტრი",


                )
        )
        reciepList.add(
            Reciep(
                6,
                "https://gemrielia.ge/media/__sized__/images/qatami_alublis_saweblit-crop-c0-5__0-5-450x301-70.jpg",
                "ალუბალი ჩავყაროთ ქვაბში, დავასხათ ძალიან ცოტა წყალი და წამოვადუღოთ, შემდეგ ბლენდერით ავთქვიფოთ, ჩავაბრუნოთ ქვაბში, შევურიოთ ცოტაოდენ ცივ წყალში გახსნილი სახამებელი და მარილი. სოუსს დავუმატოთ დანაყილი ნიორი," +
                        " დაჭრილი ქინძი და შევაგრილოთ. პარალელურად ქათმის ბარკლებს წავუსვათ ზეთი, მოვაყაროთ მარილი და შევწვათ ღუმელში ან ტაფაზე. ქათმის ბარკლები სუფრაზე ალუბლის საწებელთან ერთად მივიტანოთ.",
                "ბარკლები ალუბალში",


                )
        )
        reciepList.add(
            Reciep(
                7,
                "https://gemrielia.ge/media/__sized__/images/badrijnis_tolma-crop-c0-5__0-5-450x301-70.jpg",
                "ბადრიჯნები ორად გავჭრათ და გული ამოვუღოთ. ხორცის ფარშს დავუმატოთ ნიორი, 1 თავი ხახვი, ბრინჯი, მდუღარე წყალი, ძირა, მარილი და პილპილი, მოვზილოთ ხელით და ჩავტენოთ გულამოცლილ ბადრიჯნებში. დანარჩენი ხახვი პამიდვრის " +
                        "პიურესთან ერთად მოვშუშოთ ცოტაოდენ ზეთში, დავუმატოთ მწვანილები და სანელებლები. ქვაბში ჩავაფინოთ ჭარხლის ფოთლები, ჩავაწყოთ ბადრიჯნები, დავასხათ სოუსი და მოვხარშოთ ბრინჯის მომზადებამდე.",
                "ბადრიჯნის ტოლმა",


                )
        )
        reciepList.add(
            Reciep(
                8,
                "https://gemrielia.ge/media/__sized__/images/jigris_yaurma-crop-c0-5__0-5-450x301.png",
                "გულღვიძლი დაჭერით კუბიკებად და წამოადუღეთ, დუღილის დროს მოქაფეთ.\n" +
                        "გადაიტანეთ ქოთანში, დაასხით ღვინო, დაადეთ დაფნის ფოთოლი, წიწაკა, პილპილი, წვრილად დაჭრილი ხახვი და ჩათუთქეთ ღვინოში, როდესაც დაიშრობს, დაამატეთ კარაქი და შებრაწეთ, კარგად რომ დაიბრაწება, დაამატეთ ტომატი, 1 ლიტრა ადუღებული წყალი, წვრილად დაჭრილი მწვანილები და ნიორი.\n" +
                        "ადუღებულ 500 გრ წყალში გახსენით 3კოვზი ტომატი, მარილი.  5 წუთი წამოადუღეთ და გამორთეთ. გემრიელად მიირთვით.",
                "ჯიგრის ყაურმა",


                )
        )
        reciepList.add(
            Reciep(
                9,
                "https://gemrielia.ge/media/__sized__/images/makaroni_YBLhiAW-crop-c0-5__0-5-450x301-70.jpg",
                "ქათმის ფილე დაჭერით და ზეთდასხმულ ტაფაზე შეწვით. რომ შეიწვება, დაუმატეთ მოხარშული მაკარონი და მოურიეთ. ბოლოს პამიდვრის სოუსი დაასხით (შეგიძლიათ, უბრალოდ, პამიდორი დააბლენდეროთ), ბოლოს დაუმატეთ სუნელები, ნაჭრილი ნიორი და 2-3 წუთს თავდახურულ ტაფაზე გააჩერეთ, რომ არომატები კარგად გაუჯდეს.\n" +
                        "\n" +
                        "გემრიელად მიირთვით!",
                "მაკარონი ხორცით",


                )
        )
        reciepList.add(
            Reciep(
                10,
                "https://gemrielia.ge/media/__sized__/images/farshi_XDDYsB8-crop-c0-5__0-5-450x301-70.jpg",
                "ხახვს დააცალეთ დიდი ფენები. დანარჩენი დაჭერით და ფარშს შეურიეთ. ასევე ფარშს დაუმატეთ ოხრახუში, კეტჩუპი, 2 სკ ბარბექიუს სოუსი, დაჭრილი ჩილი წიწაკა, დაჭრილი სოკო. გააკეთეთ ხორცის ბურთები, შუაში ყველის კუბიკი ჩაუდეთ და დააგუნდავეთ. გუნდას გარშემო ხახვის ფენა შემოახვიეთ (2 ფენა ნახევარ-ნახევარს დაფარავს). " +
                        "გარშემო გრძლად და თხლად დაჭრილი ბეკონი შემოახვიეთ. გარედან ბარბექიუს სიუსი წაუსვით და გამოაცხვეთ გრილში 20 წუთით.",
                "განსხვავებული ტოლმა",


                )
        )
        reciepList.add(
            Reciep(
                11,
                "https://gemrielia.ge/media/__sized__/images/qatmisfile_ZfybQki-crop-c0-5__0-5-450x301-70.jpg",
                "ქათმის ფილე დაჭერით საშუალო ზომის ნაჭრებად. დააწყვეთ ხის ზედაპირზე, გადააფარეთ სამზარეულოს ცელოფანი და ორივე მხრიდან დაბეგვეთ.\n" +
                        "გადაიტანეთ ჯამში, დაუმატეთ კვერცხის გული, წიწაკა, პილპილი, მარილი და კარტოფილის სახამებელი. კარგად ამოურიეთ.\n" +
                        "ცილა გათქვიფეთ მწიკვ მარილთან ერთად. ღრუბელივით, ოდნავ სქელ მასას რომ მიიღებთ, საკმარისია, ძალიან არ გაამაგროთ. ქათმის ფილის ნაჭრები ამოავლეთ გათქვეფილ ცილაში და შემდეგ ცხიმში შეწვით ტაფაზე ორივე მხრიდან.",
                "ქათმის *ნაგეტსი*",


                )
        )
        reciepList.add(
            Reciep(
                12,
                "https://gemrielia.ge/media/__sized__/images/qatmisxorci-crop-c0-5__0-5-450x301-70.jpg",
                "ქათამი დაჭერით, ღრმა ტაფაზე გადაიტანეთ და წყალი დაასხით. მოაფრქვიეთ მარილი, პილპილი და სურვილისამებრ, 1 ც დაფნის ფოთოლიც დაუმატეთ. დაახურეთ თავი და საშუალო ცეცხლზე 50-60 წთ ხარშეთ. შემდეგ დაუმატეთ მდნარი ყველი და კარგად ამოურიეთ. ყველი მთლიანად რომ შეერევა ხორცს, დაუმატეთ წვრილად დაჭრილი ნიორი და ამოურიეთ. კიდევ 10 წუთით დააყოვნეთ და მზად არის.\n" +
                        "\n" +
                        "გემრიელად მიირთვით!",
                "ქათმის ბარკლები",


                )
        )
        reciepList.add(
            Reciep(
                13,
                "https://gemrielia.ge/media/__sized__/images/ludismarinadi-crop-c0-5__0-5-450x301-70.jpg",
                "მოცემული მასალა 1.5-2 კგ ხორცზეა გათვლილი.\n" +
                        "\n" +
                        "ხახვი დაჭერით მრგვლად. დააყარეთ ხორცს. დაუმატეთ დანარჩენი ინგრედიენტებიც და ამოურიეთ. სურვილისამებრ შეგიძლიათ დაუმატოთ ხორცის საკმაზიც. ჯამს თავი დაახურეთ (ან ცელოფანი გადააფარეთ) და შეინახეთ მაცივარში მთელი ღამით.",
                "მწვადის მარინადი",


                )
        )
        reciepList.add(
            Reciep(
                14,
                "https://gemrielia.ge/media/__sized__/images/qaTmisfile_H7U46iD-crop-c0-5__0-5-450x301-70.jpg",
                "ფარშს შეურიეთ მანის ბურღული, მწიკვი მარილი და პილპილი. კარგად ამოურიეთ. \n" +
                        "შიგთავსის ინგრედიენტები დაჭერით და ერთმანეთს შეურიეთ.\n" +
                        "ფარშს ნავის ფორმა მიეცით, აჭარული ხაჭაპურის მსგავსად შუაში ჩაპრესეთ, შიგთავსი მოათავსეთ და გამოაცხვეთ 200 გრადუსზე გახურებულ ღუმელში 2 წუთით. შემდეგ ცენტში მწყრის კვერცხი ჩაახალეთ და ღუმელში შეაბრუნეთ კიდევ 5 წუთით.\n" +
                        "\n" +
                        "გემრიელად მიირთვით!",
                "ფარშის ნავი",


                )
        )
        reciepList.add(
            Reciep(
                15,
                "https://gemrielia.ge/media/__sized__/images/burgeris_xorci-crop-c0-5__0-5-450x301-70.jpg",
                "კატლეტების ფორმა რომ მრგვალი და ლამაზი გამოვიდეს,გამოიყენეთ სურათზე მოცემული სილიკონის ფორმა."+
                        "ცელოფნი ჩააფინეთ ისე,რომ ყველა ღრმულში ჩაიფინოს,შემდეგ ჩადეთ ფარში,კარგად გაანაწილეთ,ცარიელი ადგილები რომ არსად იყოს,ზემოდან გადაფარეთ ცელოფნის მერე ნაწილი,კარგად დააფიქსირეთ,მოჭერით და გადაიტანეთ ლანგარზე.ასეთ " +
                        "მდგომარეობაში ჩადეთ მაცივარში, რომ უფრო გამკვრივდეს და ფორმა დაფიქსირდეს,ან გაყინვაც შეგიძლიათ, შემდეგ ცელოფანი ააცალეთ და შეწვით.",
                "ჰამბურგერის კატლეტი",


                )
        )


        return reciepList
    }

}