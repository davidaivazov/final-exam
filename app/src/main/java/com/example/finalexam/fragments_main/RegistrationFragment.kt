package com.example.finalexam.fragments_main

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.finalexam.R
import com.google.firebase.auth.FirebaseAuth

class RegistrationFragment:Fragment(R.layout.fragment_registration) {
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPasswordRepeat: EditText
    private lateinit var buttonSumbit: Button
    private lateinit var buttonBack: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editTextEmail = view.findViewById(R.id.editTextEmail)
        editTextPassword = view.findViewById(R.id.editTextPassword)
        editTextPasswordRepeat = view.findViewById(R.id.editTextPasswordRepeat)
        buttonBack = view.findViewById(R.id.buttonBack)
        buttonSumbit = view.findViewById(R.id.buttonSumbit)
        val controller = Navigation.findNavController(view)

        buttonSumbit.setOnClickListener{
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val passwordRepeat = editTextPasswordRepeat.text.toString()

            if(email.isEmpty() || password.isEmpty() || passwordRepeat.isEmpty()) {
                Toast.makeText(context,"შეიყვანეთ ყველა ველი!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if( "@" !in email ) {
                editTextEmail.setError("შეიყვანეთ რეალური ელ. ფოსტა")
                editTextEmail.requestFocus()
                editTextEmail.isEnabled = true
                return@setOnClickListener
            }
            if (password.length < 9){
                editTextPassword.setError("პაროლის მინიმალური სიგრძე: 9 სიმბოლო!")
                editTextPassword.requestFocus()
                editTextPassword.isEnabled = true
                return@setOnClickListener

            }
            if(passwordRepeat != password) {
                editTextPasswordRepeat.setError("პაროლები არ ემთხვევა ერთმანეთს!")
                editTextPasswordRepeat.requestFocus()
                editTextPasswordRepeat.isEnabled = true
                return@setOnClickListener

            }
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener{ task ->
                    if(task.isSuccessful){
                        val action =
                            RegistrationFragmentDirections.actionRegistrationFragmentToAuthorizationFragment()
                        controller.navigate(action)
                        Toast.makeText(context,"წარმატებით დარეგისტრირდა!",
                            Toast.LENGTH_SHORT).show()
                    }
                    else { Toast.makeText(context,"ვერ დარეგისტრირდა! მომხმარებელი უკვე " +
                            "არსებობს, ან რაღაცა არასწორედ არის შეტანილი!",
                        Toast.LENGTH_LONG).show()
                    }
                }
    }
        buttonBack.setOnClickListener(){
            val action =
                RegistrationFragmentDirections.actionRegistrationFragmentToAuthorizationFragment()
            controller.navigate(action)

    }

    }}