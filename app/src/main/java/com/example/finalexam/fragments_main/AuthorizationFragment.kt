package com.example.finalexam.fragments_main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.finalexam.ProfileActivity
import com.example.finalexam.R
import com.google.firebase.auth.FirebaseAuth

class AuthorizationFragment: Fragment(R.layout.fragment_authorization) {
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonSign: Button
    private lateinit var buttonRegistration: Button
    private lateinit var buttonResetPass: TextView
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        val controller = Navigation.findNavController(view)
        editTextEmail = view.findViewById(R.id.editTextEmail)
        editTextPassword = view.findViewById(R.id.editTextPassword)
        buttonSign = view.findViewById(R.id.buttonSign)
        buttonRegistration = view.findViewById(R.id.buttonRegistration)
        buttonResetPass = view.findViewById(R.id.buttonResetPass)
        buttonRegistration.setOnClickListener(){
            val action =
            AuthorizationFragmentDirections.actionAuthorizationFragmentToRegistrationFragment()
            controller.navigate(action)
        }
        buttonResetPass.setOnClickListener(){
            val action =
                AuthorizationFragmentDirections.actionAuthorizationFragmentToResetpassActivity()
            controller.navigate(action)
        }
        buttonSign.setOnClickListener(){
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            if(email.isEmpty() || password.isEmpty()) {
                Toast.makeText(context,"შეიყვანეთ ყველა ველი!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if( "@" !in email ) {
                editTextEmail.setError("შეიყვანეთ რეალური ელ. ფოსტა")
                editTextEmail.requestFocus()
                editTextEmail.isEnabled = true
                return@setOnClickListener
            }
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email,password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        startActivity(Intent(context, ProfileActivity::class.java))
                        finishAffinity(requireActivity())
                    } else {
                        editTextPassword.setError("არასწორი  პაროლი ან/და ფოსტა!")
                        editTextPassword.requestFocus()
                        editTextPassword.isEnabled = true
                    }
                }

        }
    }}
