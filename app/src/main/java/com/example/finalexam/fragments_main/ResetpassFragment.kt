package com.example.finalexam.fragments_main

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.finalexam.R
import com.google.firebase.auth.FirebaseAuth

class ResetpassFragment: Fragment(R.layout.fragment_resetpass) {
    private lateinit var editTextEmail: EditText
    private lateinit var buttonReset: Button
    private lateinit var buttonBack: Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val controller = Navigation.findNavController(view)

        editTextEmail = view.findViewById(R.id.editTextEmail)
        buttonReset = view.findViewById(R.id.buttonReset)
        buttonBack = view.findViewById(R.id.buttonBack)
        buttonReset.setOnClickListener(){
            val email = editTextEmail.text.toString()
            if (email.isEmpty()){
                editTextEmail.setError("შეიყვანეთ რეალური ელ. ფოსტა, რომლითაც ხართ დარეგისტრირებული")
                editTextEmail.requestFocus()
                editTextEmail.isEnabled = true
                return@setOnClickListener
            } else if( "@" !in email ) {
                editTextEmail.setError("შეიყვანეთ რეალური ელ. ფოსტა")
                editTextEmail.requestFocus()
                editTextEmail.isEnabled = true
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val action =
                        ResetpassFragmentDirections.actionResetpassActivityToAuthorizationFragment()
                    controller.navigate(action)
                    Toast.makeText(context, "გადაამოწმეთ თქვენი ელ. ფოსტა!", Toast.LENGTH_SHORT).show()
            }
                else{
                    editTextEmail.setError("მომხმარებელი არ მოიძებნა!!!")
                    editTextEmail.requestFocus()
                    editTextEmail.isEnabled = true
                }

            }
        }

        buttonBack.setOnClickListener(){
            val action =
                ResetpassFragmentDirections.actionResetpassActivityToAuthorizationFragment()
            controller.navigate(action)
        }
    }
}