package com.example.finalexam.fragments_profile

import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.finalexam.R


class NotesFragment:Fragment(R.layout.fragment_notes) {
    private lateinit var textNote: TextView
    private lateinit var writeNote: EditText
    private lateinit var buttonAdd: Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textNote = view.findViewById(R.id.textNote)
        writeNote = view.findViewById(R.id.writeNote)
        buttonAdd = view.findViewById(R.id.buttonAdd)
        val sharedPreferences = activity?.getSharedPreferences("MY_APP_PR", MODE_PRIVATE)
        val notes = sharedPreferences!!.getString("NOTES","")
        textNote.text = notes
        buttonAdd.setOnClickListener {
            val note = writeNote.text.toString()
            val text = textNote.text.toString()
            val result = note + "\n" + text
            if (writeNote.text.isEmpty()) {
                return@setOnClickListener
            }
            textNote.text = result
            sharedPreferences.edit()
                .putString("NOTES",result)
                .apply()
            writeNote.text.clear()
        }
    }

}