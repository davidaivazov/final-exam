package com.example.finalexam.fragments_profile

data class UserInfo(
        var url: String = "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png",
        var name: String = "User!"
)