package com.example.finalexam.fragments_profile

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.finalexam.R

class MenuFragment:Fragment(R.layout.fragment_menu) {
    private lateinit var buttonDesert: Button
    private lateinit var buttonXorceuli: Button
    private lateinit var buttonSeafood: Button
    private lateinit var buttonSalad: Button
    private lateinit var buttonSoup: Button
    private lateinit var buttonSouse: Button
    private lateinit var buttonVegeterian: Button

    private lateinit var buttonBack: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val controller = Navigation.findNavController(view)
        buttonDesert = view.findViewById(R.id.buttonDesert)
        buttonXorceuli = view.findViewById(R.id.buttonXorceuli)
        buttonSeafood = view.findViewById(R.id.buttonSeafood)
        buttonSalad = view.findViewById(R.id.buttonSalad)
        buttonSoup = view.findViewById(R.id.buttonSoup)
        buttonSouse = view.findViewById(R.id.buttonSouse)
        buttonVegeterian = view.findViewById(R.id.buttonVegeterian)
        buttonBack = view.findViewById(R.id.buttonBack)

        buttonDesert.setOnClickListener(){
            val action =
                    MenuFragmentDirections.actionMenuFragmentToDesertFragment()
                    controller.navigate(action)
        }
        buttonXorceuli.setOnClickListener(){
            val action =
                MenuFragmentDirections.actionMenuFragmentToXorceuliFragment()
            controller.navigate(action)
        }
        buttonSeafood.setOnClickListener(){
            val action =
                MenuFragmentDirections.actionMenuFragmentToSeaFoodFragment()
            controller.navigate(action)
        }
        buttonSalad.setOnClickListener(){
            val action =
                MenuFragmentDirections.actionMenuFragmentToSaladFragment()
            controller.navigate(action)
        }
        buttonSoup.setOnClickListener(){
            val action =
                MenuFragmentDirections.actionMenuFragmentToSoupFragment()
            controller.navigate(action)
        }
        buttonSouse.setOnClickListener(){
            val action =
                MenuFragmentDirections.actionMenuFragmentToSouseFragment()
            controller.navigate(action)
        }
        buttonVegeterian.setOnClickListener(){
            val action =
                MenuFragmentDirections.actionMenuFragmentToVegeterianFragment()
            controller.navigate(action)
        }
        buttonBack.setOnClickListener() {
            val action =
                    MenuFragmentDirections.actionMenuFragmentToProfileFragment()
                    controller.navigate(action)
        }
    }
}