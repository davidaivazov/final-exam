package com.example.finalexam.fragments_profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.example.finalexam.MainActivity
import com.example.finalexam.R
import com.example.finalexam.fragments_main.AuthorizationFragmentDirections
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfileFragment: Fragment(R.layout.fragment_profile) {
    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseDatabase.getInstance().getReference("UserInfo")
    private lateinit var buttonSettings: Button
    private lateinit var buttonMenu: Button
    private lateinit var buttonNotes: Button
    private lateinit var buttonLogout: Button
    private lateinit var userName: TextView
    private lateinit var userPhoto: ImageView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val controller = Navigation.findNavController(view)
        buttonSettings = view.findViewById(R.id.buttonSettings)
        buttonMenu = view.findViewById(R.id.buttonmenu)
        buttonNotes = view.findViewById(R.id.buttonNotes)
        buttonLogout = view.findViewById(R.id.buttonLogout)
        userName = view.findViewById(R.id.user_name)
        userPhoto = view.findViewById(R.id.user_image)

        db.child(auth.currentUser?.uid!!).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val userInfo = snapshot.getValue(UserInfo::class.java) ?: return

                userName.text = userInfo.name
                if (getActivity() == null) {
                    return;
                }
                Glide.with(this@ProfileFragment)
                    .load(userInfo.url)
                    .into(userPhoto)

            }

            override fun onCancelled(error: DatabaseError) {
            }
        })
        buttonSettings.setOnClickListener(){
            val action =
                   ProfileFragmentDirections.actionProfileFragmentToSettingsFragment()
            controller.navigate(action)
        }

        buttonMenu.setOnClickListener(){
            val action =
                    ProfileFragmentDirections.actionProfileFragmentToMenuFragment()
            controller.navigate(action)
        }

        buttonNotes.setOnClickListener(){
            val action =
                    ProfileFragmentDirections.actionProfileFragmentToNotesFragment()
            controller.navigate(action)
        }

        buttonLogout.setOnClickListener(){
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(context,MainActivity::class.java))
            Toast.makeText(context, "თქვენ გამოხვედით პროფილიდან!", Toast.LENGTH_SHORT).show()
            finishAffinity(requireActivity())
        }
    }
}