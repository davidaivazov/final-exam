package com.example.finalexam.fragments_profile

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.example.finalexam.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class SettingsFragment: Fragment(R.layout.fragment_settings) {

    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseDatabase.getInstance().getReference("UserInfo")
    private lateinit var editTextChangePassword: EditText
    private lateinit var buttonChange: Button
    private lateinit var buttonBack: Button
    private lateinit var changeName: EditText
    private lateinit var changePhoto: EditText
    private lateinit var buttonApply: Button
    private lateinit var userPhoto: ImageView
    private lateinit var userName: TextView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val controller = Navigation.findNavController(view)
        editTextChangePassword = view.findViewById(R.id.change_password)
        buttonBack = view.findViewById(R.id.buttonBack)
        buttonChange = view.findViewById(R.id.buttonChange)
        changeName = view.findViewById(R.id.change_name)
        changePhoto = view.findViewById(R.id.change_photo)
        buttonApply = view.findViewById(R.id.buttonApply)
        userPhoto = view.findViewById(R.id.user_image)
        userName = view.findViewById(R.id.user_name)
        buttonApply.setOnClickListener{
            if (changeName.text.isEmpty() and changePhoto.text.isEmpty()){
                return@setOnClickListener
            }
            val url = changePhoto.text.toString()
            val name = changeName.text.toString()
            val userInfo = UserInfo(url, name)
            db.child(auth.currentUser?.uid!!).setValue(userInfo)
            Toast.makeText(context,"წარმატებულად შეიცვალა",Toast.LENGTH_SHORT).show()
            changeName.text.clear()
            changePhoto.text.clear()
        }
        db.child(auth.currentUser?.uid!!).addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val userInfo = snapshot.getValue(UserInfo::class.java) ?: return
                db.child(auth.currentUser?.uid!!).setValue(userInfo)
                userName.text = userInfo.name
                if (getActivity() == null) {
                    return;
                }

                Glide.with(this@SettingsFragment)
                    .load(userInfo.url)
                    .into(userPhoto)

            }

            override fun onCancelled(error: DatabaseError) {
            }
        })

        buttonChange.setOnClickListener{
            val newPassword = editTextChangePassword.text.toString()
            if (newPassword.isEmpty()) {
                editTextChangePassword.setError("შეიყვანეთ პაროლი!")
                editTextChangePassword.requestFocus()
                editTextChangePassword.isEnabled = true
                return@setOnClickListener
            } else if (newPassword.length <9) {
                editTextChangePassword.setError("ახალი პაროლის სიგრძე მინიმუმ 9 სიმბოლოა!")
                editTextChangePassword.requestFocus()
                editTextChangePassword.isEnabled = true
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)
                    ?.addOnCompleteListener{ task ->
                        if (task.isSuccessful){
                            Toast.makeText(context,"პაროლი წარმატებულად შეიცვალა!",Toast.LENGTH_SHORT).show()
                            editTextChangePassword.text.clear()
                        }
                        else{
                            editTextChangePassword.setError("დაფიქსირდა რაღაცა შეცდომა!")
                            editTextChangePassword.requestFocus()
                            editTextChangePassword.isEnabled = true
                        }

                    }
        }
        buttonBack.setOnClickListener(){
            val action =
                    SettingsFragmentDirections.actionSettingsFragmentToProfileFragment()
            controller.navigate(action)
        }
    }
}